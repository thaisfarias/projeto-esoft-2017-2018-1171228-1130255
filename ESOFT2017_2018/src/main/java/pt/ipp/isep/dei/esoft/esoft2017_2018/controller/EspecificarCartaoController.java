/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.controller;


import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Cartao;

/**
 *
 * @author Thais Farias
 */
public class EspecificarCartaoController {
    private Empresa m_oEmpresa;
    private Cartao m_oCartao;
    public EspecificarCartaoController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoCartao()
    {
        this.m_oCartao = this.m_oEmpresa.novoCartao();
    }
    
    public void setDados(String numID,String dataEmissao,String versao)
    {
        this.m_oCartao.setNumID(numID);
        this.m_oCartao.setDataEmissao(dataEmissao);
        this.m_oCartao.setVersao(versao);
        
    }
    
    public boolean registaCartao()
    {
        return this.m_oEmpresa.registaCartao(this.m_oCartao);
    }

    public String getCartaoString()
    {
        return this.m_oCartao.toString();
    }

    public void validaCartao()
    {
        this.m_oEmpresa.validaCartao(this.m_oCartao);
    }
    
}


