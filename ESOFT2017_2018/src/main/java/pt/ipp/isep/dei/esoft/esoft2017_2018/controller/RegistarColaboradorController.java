/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.controller;

import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Colaborador;

/**
 *
 * @author Ana Jesus
 */
public class RegistarColaboradorController {

    private Empresa m_oEmpresa;
    private Colaborador m_oColaborador;

    public RegistarColaboradorController(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
    }

    public void novoColaborador() {
        this.m_oColaborador = this.m_oEmpresa.novoColaborador();
    }

    public void setDados(String snumMecanog, String snomeCompleto, String snomeAbrev, String sperfilAutor) {
        this.m_oColaborador.setNumMecanog(snumMecanog);
        this.m_oColaborador.setNomeCompleto(snomeCompleto);
        this.m_oColaborador.setNomeAbrev(snomeAbrev);
        this.m_oColaborador.setPerfilAutor(sperfilAutor);
    }

    public boolean registaColaborador() {
        return this.m_oEmpresa.registaColaborador(this.m_oColaborador);
    }

    public String getColaboradorString() {
        return this.m_oColaborador.toString();
    }

    public void validaColaborador() {
        this.m_oEmpresa.validaColaborador(this.m_oColaborador);
    }

    /**
     *
     * @param nomeAbreviado
     */
    public void setNomeAbrev(int nomeAbreviado) {
        // TODO - implement RegistarColaboradorController.setNomeAbrev
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param idP
     */
    public void setPerfil(int idP) {
        // TODO - implement RegistarColaboradorController.setPerfil
        throw new UnsupportedOperationException();
    }

}
