/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.controller;

import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Perfil;

/**
 *
 * @author Ana Jesus
 */
public class RegistarPerfilAutorizacaoController {

    private Empresa m_oEmpresa;
    private Perfil m_oPerfil;

    public RegistarPerfilAutorizacaoController(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
    }

    public void novoPerfil() {
        this.m_oPerfil = this.m_oEmpresa.novoPerfil();
    }

    public void setDados(String identificador, String descricao, String equipAutorizados, String periodoAutor) {
        this.m_oPerfil.setNumID(identificador);
        this.m_oPerfil.setDescricao(descricao);
        this.m_oPerfil.setEquipAutorizados(equipAutorizados);
        this.m_oPerfil.setPeriodoAutor(periodoAutor);

    }

    public boolean registaPerfil() {
        return this.m_oEmpresa.registaPerfil(this.m_oPerfil);
    }

    public String getPerfilString() {
        return this.m_oPerfil.toString();
    }

    public void validaPerfil() {
        this.m_oEmpresa.validaPerfil(this.m_oPerfil);
    }

    /**
     *
     * @param idEquipamento
     */
    public void addEquipamento(int idEquipamento) {
        // TODO - implement RegistarPerfilDeAutorizacaoController.addEquipamento
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param periodo
     */
    public void addPeriodo(int periodo) {
        // TODO - implement RegistarPerfilDeAutorizacaoController.addPeriodo
        throw new UnsupportedOperationException();
    }

}
