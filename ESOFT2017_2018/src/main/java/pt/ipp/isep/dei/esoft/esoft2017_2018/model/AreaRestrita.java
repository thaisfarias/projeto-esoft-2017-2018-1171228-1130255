/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

/**
 *
 * @author Thais Farias
 */
public class AreaRestrita {

    private int lotacao;
    private String codigo;
    private String descricao;
    private String localizacao;

    public AreaRestrita(int lotacao, String codigo, String descricao, String localizacao) {
        this.lotacao = lotacao;
        this.codigo = codigo;
        this.descricao = descricao;
        this.localizacao = localizacao;
    }

}
