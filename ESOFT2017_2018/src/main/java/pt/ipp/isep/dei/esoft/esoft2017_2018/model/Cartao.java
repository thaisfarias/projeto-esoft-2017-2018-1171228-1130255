/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

/**
 *
 * @author Thaís Farias
 */
public class Cartao {
    
    // número único de identificação, uma data de emissão, uma versão
    private String m_numID;
    private String m_dataEmissao;
    private String m_versao;
    
    public Cartao(){
    }
    
    public Cartao(String numID, String dataEmissao, String versao) {
        this.setNumID(numID);
        this.setDataEmissao(dataEmissao);
        this.setVersao(versao);
       
    }

    public final void setNumID(String numID) {
        this.m_numID = numID;
    }

    public final void setDataEmissao( String dataEmissao) {
        this.m_dataEmissao = dataEmissao;
    }
    
     public final void setVersao( String versao) {
        this.m_versao = versao;
    }

    public String getVersao() {
        return this.m_versao;
    }

    public String getDataEmissaoo() {
        return this.m_dataEmissao;
    }

    public String getNumID() {
        return this.m_numID;
    }

   
    public boolean valida() {

        if (m_numID == null || m_numID.equals("")|| m_versao == null || m_versao.equals("") || m_dataEmissao == null || m_dataEmissao.equals("")) {
            return false;
        } else {
            return true;

        }
    }

   public boolean isIdentifiableAs(String sID) {
        return this.m_numID.equals(sID);
    } 
   
   public String getCartao() {
		// TODO - implement Cartao.getCartao
		throw new UnsupportedOperationException();
	}


   

    @Override
    public String toString() {
        return this.m_numID + ";" + this.m_versao + ";" + this.m_dataEmissao + ";";
    }


}
