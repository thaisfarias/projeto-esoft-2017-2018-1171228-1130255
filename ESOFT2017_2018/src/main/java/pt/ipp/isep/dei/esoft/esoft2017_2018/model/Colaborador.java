/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

/**
 *
 * @author Ana Jesus
 */
public class Colaborador {

    //número mecanográfico, um nome completo, um nome abreviado e um perfil de autorização
    private String m_snumMecanog;
    private String m_snomeCompleto;
    private String m_snomeAbrev;
    private String m_sperfilAutor;

    public Colaborador() {
    }

    public Colaborador(String snumMecanog, String snomeCompleto, String snomeAbrev, String sperfilAutor) {
        this.setNumMecanog(snumMecanog);
        this.setNomeCompleto(snomeCompleto);
        this.setNomeAbrev(snomeAbrev);
        this.setPerfilAutor(sperfilAutor);
    }

    public String getNumMecanog() {
        return m_snumMecanog;
    }

    public final void setNumMecanog(String snumMecanog) {
        this.m_snumMecanog = snumMecanog;
    }

    public String getNomeCompleto() {
        return m_snomeCompleto;
    }

    public final void setNomeCompleto(String snomeCompleto) {
        this.m_snomeCompleto = snomeCompleto;
    }

    public String getNomeAbrev() {
        return m_snomeAbrev;
    }

    public final void setNomeAbrev(String snomeAbrev) {
        this.m_snomeAbrev = snomeAbrev;
    }

    public String getPerfilAutor() {
        return m_sperfilAutor;
    }

    public final void setPerfilAutor(String sperfilAutor) {
        this.m_sperfilAutor = sperfilAutor;
    }
    
    public boolean valida() {

        if (m_snumMecanog == null || m_snumMecanog.equals("") || m_snomeCompleto == null || m_snomeCompleto.equals("") || m_snomeAbrev == null || m_snomeAbrev.equals("") || m_sperfilAutor == null || m_sperfilAutor.equals("")) {
            return false;
        } else {
            return true;

        }
    }
       public boolean isIdentifiableAs(String sID) {
        return this.m_snumMecanog.equals(sID);
    }
       
    @Override
    public String toString() {
        return this.m_snumMecanog + ";" + this.m_snomeCompleto + ";" + this.m_snomeAbrev + ";" + this.m_sperfilAutor + ";";
    }
    
}

