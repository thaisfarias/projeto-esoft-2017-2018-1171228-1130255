/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Empresa {

    private final List<Equipamento> m_lstEquipamentos;
    private final List<Cartao> m_lstCartoes;
    private final List<Perfil> m_lstPerfil;
    private final List<Colaborador> m_lstColaborador;

    public Empresa() {
        this.m_lstEquipamentos = new ArrayList<Equipamento>();
        this.m_lstCartoes = new ArrayList<Cartao>();
        this.m_lstColaborador = new ArrayList<Colaborador>();
        this.m_lstPerfil = new ArrayList<Perfil>();
        fillInData();
    }

    private void fillInData() {
        // Dados de Teste
        //Preenche alguns Terminais
        for (Integer i = 1; i <= 4; i++) {
            addEquipamento(new Equipamento("Equipamento " + i.toString(), "0" + i.toString(), "", ""));
        }

        //Preencher outros dados aqui
    }

    public Equipamento novoEquipamento() {
        return new Equipamento();
    }

    public boolean validaEquipamento(Equipamento e) {
        boolean bRet = false;
        if (e.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaEquipamento(Equipamento e) {
        if (this.validaEquipamento(e)) {
            return addEquipamento(e);
        }
        return false;
    }

    private boolean addEquipamento(Equipamento e) {
        return m_lstEquipamentos.add(e);
    }

    public List<Equipamento> getListaEquipamentos() {
        return this.m_lstEquipamentos;
    }

    public Equipamento getEquipamento(String sEquipamento) {
        for (Equipamento term : this.m_lstEquipamentos) {
            if (term.isIdentifiableAs(sEquipamento)) {
                return term;
            }
        }

        return null;
    }

    public Cartao novoCartao() {
        return new Cartao();
    }

    public boolean validaCartao(Cartao c) {
        boolean bRet = false;
        if (c.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaCartao(Cartao c) {
        if (this.validaCartao(c)) {
            return addCartao(c);
        }
        return false;
    }

    private boolean addCartao(Cartao c) {
        return m_lstCartoes.add(c);
    }

    public List<Cartao> getListaCartao() {
        return this.m_lstCartoes;
    }

    public Cartao getCartao(String sCartao) {
        for (Cartao term : this.m_lstCartoes) {
            if (term.isIdentifiableAs(sCartao)) {
                return term;
            }
        }

        return null;
    }

    public Perfil novoPerfil() {
        return new Perfil();
    }

    public boolean validaPerfil(Perfil p) {
        boolean bRet = false;
        if (p.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaPerfil(Perfil p) {
        if (this.validaPerfil(p)) {
            return addPerfil(p);
        }
        return false;
    }

    private boolean addPerfil(Perfil p) {
        return m_lstPerfil.add(p);
    }

    public List<Perfil> getListaPerfil() {
        return this.m_lstPerfil;
    }

    public Perfil getPerfil(String p) {
        for (Perfil term : this.m_lstPerfil) {
            if (term.isIdentifiableAs(p)) {
                return term;
            }
        }

        return null;
    }

    public Colaborador novoColaborador() {
        return new Colaborador();
    }

    public boolean validaColaborador(Colaborador colab) {
        boolean bRet = false;
        if (colab.valida()) {
            // Escrever aqui o código de validação

            //
            bRet = true;
        }
        return bRet;
    }

    public boolean registaColaborador(Colaborador colab) {
        if (this.validaColaborador(colab)) {
            return addColaborador(colab);
        }
        return false;
    }

    private boolean addColaborador(Colaborador colab) {
        return m_lstColaborador.add(colab);
    }

    public List<Colaborador> getListaColaborador() {
        return this.m_lstColaborador;
    }

    public Colaborador getColaborador(String sColaborador) {
        for (Colaborador term : this.m_lstColaborador) {
            if (term.isIdentifiableAs(sColaborador)) {
                return term;
            }
        }

        return null;
    }

    
}
