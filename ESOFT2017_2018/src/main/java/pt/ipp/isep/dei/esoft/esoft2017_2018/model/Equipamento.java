/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

import java.util.ArrayList;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Equipamento {

    private String m_sDescricao;
    private String m_sEnderecoLogico;
    private String m_sEnderecoFisico;
    private String m_sFicheiroConfiguracao;

    public Equipamento() {
    }

    public Equipamento(String sDescricao, String sEnderecoLogico, String sEnderecoFisico, String sFicheiroConfiguracao) {
        this.setDescricao(sDescricao);
        this.setEnderecoLogico(sEnderecoLogico);
        this.setEnderecoFisico(sEnderecoFisico);
        this.setFicheiroConfiguracao(sFicheiroConfiguracao);
    }

    public final void setDescricao(String sDescricao) {
        this.m_sDescricao = sDescricao;
    }

    public final void setEnderecoLogico(String sEnderecoLogico) {
        this.m_sEnderecoLogico = sEnderecoLogico;
    }

    public String getEnderecoLogico() {
        return this.m_sEnderecoLogico;
    }

    public final void setEnderecoFisico(String sEnderecoFisico) {
        this.m_sEnderecoFisico = sEnderecoFisico;
    }

    public final void setFicheiroConfiguracao(String sFicheiroConfiguracao) {
        this.m_sFicheiroConfiguracao = sFicheiroConfiguracao;
    }

    /**
     *
     * @author Ana Jesus
     * @return
     */
    public String getEnderecoFisico() {
        return this.m_sEnderecoFisico;
    }

    public String getFicheiroConfiguracao() {
        return this.m_sFicheiroConfiguracao;
    }

    public String getDescricao() {
        return this.m_sDescricao;
    }

    public boolean valida() {

        if (m_sEnderecoFisico == null || m_sEnderecoFisico.equals("") || m_sDescricao == null || m_sDescricao.equals("") || m_sFicheiroConfiguracao == null || m_sFicheiroConfiguracao.equals("") || m_sEnderecoLogico == null || m_sEnderecoLogico.equals("")) {
            return false;
        } else {
            return true;

        }
    }

    public boolean isIdentifiableAs(String sID) {
        return this.m_sEnderecoLogico.equals(sID);
    }

    @Override
    public String toString() {
        return this.m_sDescricao + ";" + this.m_sEnderecoLogico + ";" + this.m_sEnderecoFisico + ";" + this.m_sFicheiroConfiguracao + ";";
    }

    public void setArea(String area) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setMovimento(String movimento) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getModelo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList procuraEquipamentosAutorizados() {
        // TODO - implement Equipamento.procuraEquipamentosAutorizados
        throw new UnsupportedOperationException();
    }

    public void enviaCartoesAutorizados() {
        // TODO - implement Equipamento.enviaCartoesAutorizados
        throw new UnsupportedOperationException();
    }

}
