/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

/**
 *
 * @author Thaís Farias
 */
public class Perfil {

    // número único de identificação, uma descricao, uma equipamentos
    private String m_numID;
    private String m_descricao;
    private String m_equipAutorizados;
    private String m_periodoAutor;

    public Perfil() {
    }

    public Perfil(String numID, String descricao, String equipAutorizados, String periodoAutor) {
        this.setNumID(numID);
        this.setDescricao(descricao);
        this.setEquipAutorizados(equipAutorizados);
        this.setPeriodoAutor(periodoAutor);

    }

    public final void setNumID(String numID) {
        this.m_numID = numID;
    }

    public final void setDescricao(String descricao) {
        this.m_descricao = descricao;
    }

    public final void setEquipAutorizados(String equipAutorizados) {
        this.m_equipAutorizados = equipAutorizados;
    }

    public final void setPeriodoAutor(String PeriodoAutor) {
        this.m_periodoAutor = PeriodoAutor;
    }

    public String getDescricao() {
        return this.m_descricao;
    }

    public String getEquipAutorizados() {
        return this.m_equipAutorizados;
    }

    public String getNumID() {
        return this.m_numID;
    }

    public String getNPeriodoAutor() {
        return this.m_periodoAutor;
    }
    
    	/**
	 * 
	 * @param e
	 * @param periodo
	 */
	public void adicionaPeriodo(int e, int periodo) {
		// TODO - implement PerfilDeAutorizacao.adicionaPeriodo
		throw new UnsupportedOperationException();
	}


    public boolean valida() {

        if (m_numID == null || m_numID.equals("") || m_descricao == null || m_descricao.equals("") || m_equipAutorizados == null || m_equipAutorizados.equals("")) {
            return false;
        } else {
            return true;

        }
    }

    public boolean isIdentifiableAs(String sID) {
        return this.m_numID.equals(sID);
    }

    @Override
    public String toString() {
        return this.m_numID + ";" + this.m_descricao + ";" + this.m_equipAutorizados + ";" + this.m_periodoAutor + ";";
    }

}
