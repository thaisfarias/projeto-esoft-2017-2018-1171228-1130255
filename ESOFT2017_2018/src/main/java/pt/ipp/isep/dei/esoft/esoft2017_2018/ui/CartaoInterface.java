/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Colaborador;

/**
 *
 * @author Ana Jesus
 */
public interface CartaoInterface {

    /**
     *
     * @param cartao_id
     */
    boolean isCartao(String cartao_id);

    /**
     *
     * @param colaborador
     * @param dataInicio
     */
    AtribuicaoCartaoUi novaAtribuicao(Colaborador colaborador, long dataInicio);

    /**
     *
     * @param novaAC
     */
    boolean validaAtribuicao(AtribuicaoUI novaAC);

    /**
     *
     * @param novaAc
     */
    boolean registaAtribuicao(AtribuicaoCartaoUi novaAc);

    /**
     *
     * @param dataFim
     */
    void fimAntigaAtribuicao(int dataFim);

}
