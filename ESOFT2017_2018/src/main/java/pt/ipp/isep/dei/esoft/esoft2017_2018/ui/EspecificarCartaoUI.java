/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import java.util.Scanner;
import pt.ipp.isep.dei.esoft.esoft2017_2018.controller.EspecificarCartaoController;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.utils.Utils;

/**
 *
 * @author Ana Jesus
 */
public class EspecificarCartaoUI {

    private Empresa m_oEmpresa;
    private EspecificarCartaoController m_controller;

    public EspecificarCartaoUI(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarCartaoController(oEmpresa);
    }

    public void run() {
        System.out.println("\nNovo Cartão:");
        m_controller.novoCartao();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Cartão? (S/N)")) {
            if (m_controller.registaCartao()) {
                System.out.println("Cartão registado.");
            } else {
                System.out.println("Cartão não registado.");
            }
        }
    }

    private void introduzDados() {
        String numID = Utils.readLineFromConsole("\"Introduza Número de Identificação: \"");
        String versao = Utils.readLineFromConsole("Introduza Versão: ");
        String dataEmissao = Utils.readLineFromConsole("Introduza data de emissão: ");

        m_controller.setDados(numID, versao, dataEmissao);
        m_controller.validaCartao();
    }

    private void apresentaDados() {
        System.out.println("\nCartao:\n" + m_controller.getCartaoString());
    }

}
