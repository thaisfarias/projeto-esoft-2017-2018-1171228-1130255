/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import java.io.IOException;
import java.util.Scanner;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MenuUI {

    private Empresa m_empresa;
    private int opcao = 0;
    Scanner read = new Scanner(System.in);

    public MenuUI(Empresa empresa) {
        m_empresa = empresa;
    }

    public void run() throws IOException {
        //int opcao = 0;
        //Scanner read = new Scanner(System.in);
        do {
            System.out.println("Escolha uma opção\n"
                    + "\n\t 1 - Especificar Equipamento"
                    + "\n\t 2 - Especificar Cartão"
                    + "\n\t 3 - Registar Perfil Autorização"
                    + "\n\t 4 - Registar Colaborador"
                    + "\n\t 5 - Atribuir Cartão"
                    + "\n\t 6 - Aceder Área Restrita"
                    + "\n\t 0 - Sair");
            String opcaoval = read.nextLine();

            if (opcaoval.length() != 1) {
                System.out.println("Por favor introduza um número válido!");
                run();
                break;
            } else {
                if (!opcaoval.equals("0") && !opcaoval.equals("1") && !opcaoval.equals("2") && !opcaoval.equals("3") && !opcaoval.equals("4") && !opcaoval.equals("5") && !opcaoval.equals("6")) {
                    System.out.println("Por favor introduza um número válido!");

                    run();
                    break;
                }
                switch (opcao) {
                    case 1:
                        EspecificarEquipamentoUI uc1 = new EspecificarEquipamentoUI(m_empresa);
                        uc1.run();
                        break;
                    case 2:
                        EspecificarCartaoUI uc2 = new EspecificarCartaoUI(m_empresa);
                        uc2.run();
                        break;
                    case 3:
                        RegistarPerfilAutorizaçãoUI uc3 = new RegistarPerfilAutorizaçãoUI(m_empresa);
                        uc3.run();
                        break;
                    case 4:
                        RegistarColaboradorUI uc4 = new RegistarColaboradorUI(m_empresa);
                        uc4.run();
                        break;
                    case 5:

                    case 6:

                }
            }

        } while (opcao < 0 || opcao > 7);
    }
}
