/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import pt.ipp.isep.dei.esoft.esoft2017_2018.controller.RegistarColaboradorController;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.utils.Utils;

/**
 *
 * @author Ana Jesus
 */
public class RegistarColaboradorUI{
    
    private Empresa m_oEmpresa;
    private RegistarColaboradorController m_controller;

    public RegistarColaboradorUI( Empresa oEmpresa ){
        this.m_oEmpresa = oEmpresa;
        m_controller = new RegistarColaboradorController(oEmpresa);
    }

    public void run()
    {
        System.out.println("\nNovo Colaborador:");
        m_controller.novoColaborador();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Colaborador? (S/N)")) {
            if (m_controller.registaColaborador()) {
                System.out.println("Colaborador registado.");
            } else {
                System.out.println("Colaborador não registado.");
            }
        }
    }
    
    private void introduzDados() {
        String snumMecanog = Utils.readLineFromConsole("Introduza número mecanográfico: ");
        String snomeCompleto = Utils.readLineFromConsole("Introduza nome completo: ");
        String snomeAbrev = Utils.readLineFromConsole("Introduza noma abreviado: ");
        String sperfilAutor = Utils.readLineFromConsole("Introduza Fperfil de autorização: ");
       
        m_controller.setDados(snumMecanog, snomeCompleto, snomeAbrev, sperfilAutor);
        m_controller.validaColaborador();
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nColaborador:\n" + m_controller.getColaboradorString());
    }
 
}


