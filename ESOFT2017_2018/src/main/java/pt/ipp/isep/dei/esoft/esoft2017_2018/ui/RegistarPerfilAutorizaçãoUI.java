/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.esoft2017_2018.ui;

import pt.ipp.isep.dei.esoft.esoft2017_2018.controller.RegistarPerfilAutorizacaoController;
import pt.ipp.isep.dei.esoft.esoft2017_2018.model.Empresa;
import pt.ipp.isep.dei.esoft.esoft2017_2018.utils.Utils;

/**
 *
 * @author Ana Jesus
 */
public class RegistarPerfilAutorizaçãoUI {

    private Empresa m_oEmpresa;
    private RegistarPerfilAutorizacaoController m_controller;
    private String sPeriodoAutor;

    public RegistarPerfilAutorizaçãoUI(Empresa oEmpresa) {
        this.m_oEmpresa = oEmpresa;
        m_controller = new RegistarPerfilAutorizacaoController(oEmpresa);
    }

    public void run() {
        System.out.println("\nNovo Perfil:");
        m_controller.novoPerfil();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Perfil? (S/N)")) {
            if (m_controller.registaPerfil()) {
                System.out.println("Perfil criado.");
            } else {
                System.out.println("Perfil não criado.");
            }
        }
    }

    private void introduzDados() {
        String snumID = Utils.readLineFromConsole("Introduza um identificador: ");
        String sDescricao = Utils.readLineFromConsole("Introduza uma descrição: ");
        String sEquipAutorizados = Utils.readLineFromConsole("Introduza os equipamentos autorizados: ");
        String sPeriodoAutor = Utils.readLineFromConsole("Introduza os periodos autorizados: ");
        
        m_controller.setDados(snumID, sDescricao, sEquipAutorizados, sPeriodoAutor);
        m_controller.validaPerfil();
    }

    private void apresentaDados() {
        System.out.println("\nPerfil:\n" + m_controller.getPerfilString());
    }


}
